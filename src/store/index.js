import Vue from 'vue'

import vuex from 'vuex'

Vue.use(vuex)

export default new vuex.Store({
    state:{
        token:localStorage.getItem('token')||'',
        userName:localStorage.getItem('userName')||'',
    },
    mutations:{
        setToken(state,data){
            state.token=data
            localStorage.setItem('token',data)
        },
        setUserName(state,data){
            state.userName = data
            localStorage.setItem('userName',data)
        }
    }
})