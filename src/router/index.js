import Vue from 'vue'
import Router from 'vue-router'
import LogIn from '@/views/LogIn'
import Home from '@/views/Home'
import store from '@/store'
import Users from '@/views/users/Users'
import Roles from '@/views/rights/Roles'

const originalPush = Router.prototype.push
Router.prototype.push = function push(location) {
  return originalPush.call(this, location).catch(err => err)
}

Vue.use(Router)

const routes = new Router({
  routes: [
    {
      path: '/',
      redirect: '/login'
    },
    {
      path: '/login',
      name: 'login',
      component: LogIn,
      meta: { requiresAuth: true }
    },
    {
      path: '/home',
      name: 'home',
      component: Home,
      children: [
        {
          path: 'users',
          name: 'users',
          component: Users
        },
        {
          path: 'roles',
          name: 'roles',
          component: Roles
        }
      ]
    }
  ],
  linkActiveClass: 'actived-class'
})

routes.beforeEach((to, from, next) => {
  if(!to.meta.requiresAuth) {
    if(store.state.token){
      next()
    }else {
      next({
        path: '/login'
      })
    }
  }else {
    next()
  }
})

export default routes