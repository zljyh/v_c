import axios from 'axios'
import { Message } from 'element-ui'
import store from '@/store'
import router from '@/router'
axios.defaults.baseURL = 'http://localhost:8888/api/private/v1/'
// export default function http(url, data, method, params){
    // return new Promise((resolve, reject) => {
    //     axios({
    //         url,
    //         data,
    //         method,
    //         params

    //     })
    //     .then(res => {
    //         if (res.data.meta.status >= 200 && res.data.meta.status < 300) {
    //             resolve(res.data)
    //         }else {
    //             Message.error(res.data.meta.msg)
    //             reject(res.data.meta)
    //         }
    //     }).catch(err => {
    //         Message.error(err)
    //         reject(err)
    //     })
    // })
//     return axios({
//         url,
//         data,
//         method,
//         params
//     })
//     .then(res => {
//         if (res.data.meta.status >= 200 && res.data.meta.status < 300) {
//             return Promise.resolve(res.data)
//         }else {
//             Message.error(res.data.meta.msg)
//             return Promise.reject(res.data.meta)
//         }
//     }, (err) => {
//         Message.error(err)
//             return Promise.reject(err)
//     })

// }

axios.interceptors.request.use(function(config){
    //在发送请求之前做些什么
    if(store.state.token){
    config.headers.Authorization=store.state.token
    }
    return config;
},function (error){
    //对请求错误做些什么
    return Promise.reject(error);
});

//添加响应拦截器
axios.interceptors.response.use(function (response){
    //对响应数据做些什么
    if(response.data.meta.msg==="无效token"){
    router.replace({
        name:'login'
     })
    }
    return response;
},function(error){
    //对响应错误做点什么
    return Promise.reject(error);
})




export default async function http(url, data, method, params) {
    try {
        let res = await axios({
            url,
            data,
            method,
            params
        })
        if (res.data.meta.status >= 200 && res.data.meta.status < 300) {
            return Promise.resolve(res.data)
        }else {
            Message.error(res.data.meta.msg)
            return Promise.reject(res.data.meta)
        }

    }catch(err) {
        Message.error(err)
        return Promise.reject(err)
    }
}
