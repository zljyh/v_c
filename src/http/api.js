import http from './index'

// function logIn(data) {
//     return http('login', data, 'post')
// }

// function getMenus() {
//     return http('menus')
// }


// export default {
//     logIn,
//     getMenus
// }


export const logIn = function(data) {
    return http('login', data, 'post')
}

export const getMenus = function() {
    return http('menus')
}

